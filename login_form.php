
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>login_form</title>
  <!-- Bootstrap core CSS-->
  <link href="/search_box/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <!-- Custom fonts for this template-->
  <link href="/search_box/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="/search_box/css/sb-admin.css" rel="stylesheet" type="text/css">
  
</head>
<body onload="firstFocus();">
  <div class="container">
    <br><br>
    
    <div class="card card-login mx-auto mt-5">
      <div align="center" class="card-header"><h4>GENEX OMS</h4>
      </div>
      <div class="card-body">
          <!-- <form name="login_form" action="login_form.php" method="post" onload="firstfocus();"> login form -->
        <form  name="login_form" action="login.php" method="post" id="form1"> <!-- login form -->
          <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email" name="email" maxlength="255" onblur="Businessemail();" ><span id="emailerror"></span>
          </div>
          
          <div class="form-group">
			  		<label for="Password">Password</label>
			      <input type="password" class="form-control" id="pwd1" name="password" maxlength="128" onblur="Pwd1();" onkeyup="checkloginform();"><span id="pwderror1"></span>
        </div>
          <div>
            <!-- <a class="btn btn-info btn-lg btn-block" href="Dashboard.html.html">SIGN IN TO ACCOUNT</a> -->
            <input class="btn btn-info btn-lg btn-block" type="submit" id="sbutton" value="SIGN IN" disabled="disabled"> 

            <!-- <span><a class=" small mt-3" href="register.html">Create Account</a></span> -->
            <span><a class="pull-right small" href="forget_password.php">Forget Password?</a></span>
       </div>
        </form>
      </div>
    </div>
  </div><br>
  <?php if(isset($_GET['login'])){
          echo "<div class='text-center text-danger'>You are already logged in from different device or browser on "
                .date('d/m/Y',strtotime($_GET['time']))." at ".date('h:i a',strtotime($_GET['time']))."</div>";
          echo "<div class='text-center text-danger'>You need to log out from that device or browser then try to re-login.</div>";
        }
    ?>
    <?php 
        if(isset($_GET['error'])){
          if($_GET['error'] == '1'){
            echo "<div class='text-center text-danger'>Invalid Email or Password.</div>";
          }else if($_GET['error'] == '2'){
            echo "<div class='text-center text-danger'>Something went wrong. Please Try again!</div>";
          }
        }
    ?>
    <?php if(isset($_GET['success'])){
          echo "<div class='text-center text-success'>Email has been sent along with instruction to reset your password.</div>";
        }
    ?>
  <!-- Bootstrap core JavaScript-->
  <script src="../vendor/jquery/jquery.min.js"></script>
  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>
  <!-- Custom JavaScript-->
  <script src="../js/login.js"></script>
  
</body>

</html>
