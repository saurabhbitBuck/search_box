

// ============== JS FOR PRICE MODAL ==================//
function price_name(){ //validate  name field in modal
    var name = document.getElementById("pricename").value;
    if(name.length == 0)
        {
            document.getElementById("pricename").style =" border:1px solid red ";
            document.getElementById("priceerror").innerHTML=" Name is required";
            document.getElementById("priceerror").style="visibility:visible;color:red";
            document.getElementById("pricename").value;
            return false;
        }
          document.getElementById("pricename").style=" ";
          document.getElementById("pricename").value;
          document.getElementById("priceerror").style="display:none";
          return true;
}
function  checkpricemodal(){ //for button disabled and enable

if(price_name() == true)
{
      document.getElementById("pricebtn").disabled = false;
}
else
{
  document.getElementById("pricebtn").disabled = true;
}
}   
// ==============END JS FOR  PRICE MODAL ==================//

// ============== JS FOR NEW PRICE MODAL ==================//
function newpricename(){ //validate  name field in modal
        var name = document.getElementById("npricename").value;
        if(name.length == 0)
            {
                document.getElementById("npricename").style =" border:1px solid red ";
                document.getElementById("npriceerror").innerHTML=" Name is required";
                document.getElementById("npriceerror").style="visibility:visible;color:red";
                document.getElementById("npricename").value;
                return false;
            }
              document.getElementById("npricename").style=" ";
              document.getElementById("npricename").value;
              document.getElementById("npriceerror").style="display:none";
              return true;
    }
function  checknpricemodal(){ //for button disabled and enable

    if(newpricename() == true)
    {
          document.getElementById("npricebtn").disabled = false;
    }
    else
    {
      document.getElementById("npricebtn").disabled = true;
    }
}   
// ==============END JS FOR NEW PRICE MODAL ==================//

// ============== JS FOR NEW TAX TYPES MODAL ==================//
function newtaxname(){ //validate  name field in modal
    var name = document.getElementById("ntaxname").value;
    if(name.length == 0)
        {
            document.getElementById("ntaxname").style =" border:1px solid red ";
            document.getElementById("ntaxerror").innerHTML=" Name is required";
            document.getElementById("ntaxerror").style="visibility:visible;color:red";
            document.getElementById("ntaxname").value;
            return false;
        }
          document.getElementById("ntaxname").style=" ";
          document.getElementById("ntaxname").value;
          document.getElementById("ntaxerror").style="display:none";
          return true;
}
function newtaxcode(){ //validate code field in modal
    
    var code = document.getElementById("ntaxcode").value;
    if(code.length == 0)
        {
            document.getElementById("ntaxcode").style=" border:1px solid red ";
            document.getElementById("ntaxcodeerror").innerHTML="Code is required";
            document.getElementById("ntaxcodeerror").style="visibility:visible;color:red";
            document.getElementById("ntaxcode").value;
            return false;
        }
            document.getElementById("ntaxcode").style=" ";
            document.getElementById("ntaxcode").value;
            document.getElementById("ntaxcodeerror").style="display:none";
            return true;
}

function  checkntaxmodal(){ //for button disabled and enable

    if(newtaxname() == true && newtaxcode() == true)
    {
        document.getElementById("ntaxbtn").disabled = false;
    }
    else
    {
    document.getElementById("ntaxbtn").disabled = true;
    }
}   
// ==============END JS FOR NEW TAX TYPES MODAL ==================//

// ============== JS FOR TAX TYPES MODAL ==================//
function tax_name(){ //validate  name field in modal
    var name = document.getElementById("taxname").value;
    if(name.length == 0)
        {
            document.getElementById("taxname").style =" border:1px solid red ";
            document.getElementById("taxerror").innerHTML=" Name is required";
            document.getElementById("taxerror").style="visibility:visible;color:red";
            document.getElementById("taxname").value;
            return false;
        }
          document.getElementById("taxname").style=" ";
          document.getElementById("taxname").value;
          document.getElementById("taxerror").style="display:none";
          return true;
}
function tax_code(){ //validate code field in modal
    
    var code = document.getElementById("taxcode").value;
    if(code.length == 0)
        {
            document.getElementById("taxcode").style=" border:1px solid red ";
            document.getElementById("taxcodeerror").innerHTML="Code is required";
            document.getElementById("taxcodeerror").style="visibility:visible;color:red";
            document.getElementById("taxcode").value;
            return false;
        }
            document.getElementById("taxcode").style=" ";
            document.getElementById("taxcode").value;
            document.getElementById("taxcodeerror").style="display:none";
            return true;
}

function  checktaxmodal(){ //for button disabled and enable

    if(tax_name() == true && tax_code() == true)
    {
        document.getElementById("taxbtn").disabled = false;
    }
    else
    {
    document.getElementById("taxbtn").disabled = true;
    }
}   
// ==============END JS FOR TAX TYPES MODAL ==================//

// ============== JS FOR PAYMENT METHODS MODAL ==================//
function payment_name(){ //validate  name field in modal
    var name = document.getElementById("paymentname").value;
    if(name.length == 0)
        {
            document.getElementById("paymentname").style =" border:1px solid red ";
            document.getElementById("paymenterror").innerHTML=" Name is required";
            document.getElementById("paymenterror").style="visibility:visible;color:red";
            document.getElementById("paymentname").value;
            return false;
        }
          document.getElementById("paymentname").style=" ";
          document.getElementById("paymentname").value;
          document.getElementById("paymenterror").style="display:none";
          return true;
}
function  checkpaymentmodal(){ //for button disabled and enable

if(payment_name() == true)
{
      document.getElementById("paymentbtn").disabled = false;
}
else
{
  document.getElementById("paymentbtn").disabled = true;
}
}   
// ============== END JS FOR PAYMENT METHODS MODAL ==================//

// ============== JS FOR NEW PAYMENT METHODS MODAL ==================//
function newpaymentname(){ //validate  name field in modal
    var name = document.getElementById("npaymentname").value;
    if(name.length == 0)
        {
            document.getElementById("npaymentname").style =" border:1px solid red ";
            document.getElementById("npaymenterror").innerHTML=" Name is required";
            document.getElementById("npaymenterror").style="visibility:visible;color:red";
            document.getElementById("npaymentname").value;
            return false;
        }
          document.getElementById("npaymentname").style=" ";
          document.getElementById("npaymentname").value;
          document.getElementById("npaymenterror").style="display:none";
          return true;
}
function  checknpaymentmodal(){ //for button disabled and enable

if(newpaymentname() == true)
{
      document.getElementById("npaymentbtn").disabled = false;
}
else
{
  document.getElementById("npaymentbtn").disabled = true;
}
}   
// ============== END JS NEW FOR PAYMENT METHODS MODAL ==================//

// ============== JS FOR STOCK MODAL ==================//
function stock_name(){ //validate  name field in modal
    var name = document.getElementById("stockname").value;
    if(name.length == 0)
        {
            document.getElementById("stockname").style =" border:1px solid red ";
            document.getElementById("stockerror").innerHTML=" Name is required";
            document.getElementById("stockerror").style="visibility:visible;color:red";
            document.getElementById("stockname").value;
            return false;
        }
          document.getElementById("stockname").style=" ";
          document.getElementById("stockname").value;
          document.getElementById("stockerror").style="display:none";
          return true;
}
function  checkstockmodal(){ //for button disabled and enable

if(stock_name() == true)
{
      document.getElementById("stockbtn").disabled = false;
}
else
{
  document.getElementById("stocksbtn").disabled = true;
}
}   
// ============== END JS FOR STOCK MODAL ==================//

// ============== JS FOR NEW STOCK MODAL ==================//
function newstockname(){ //validate  name field in modal
    var name = document.getElementById("nstockname").value;
    if(name.length == 0)
        {
            document.getElementById("nstockname").style =" border:1px solid red ";
            document.getElementById("nstockerror").innerHTML=" Name is required";
            document.getElementById("nstockerror").style="visibility:visible;color:red";
            document.getElementById("nstockname").value;
            return false;
        }
          document.getElementById("nstockname").style=" ";
          document.getElementById("nstockname").value;
          document.getElementById("nstockerror").style="display:none";
          return true;
}
function  checknstockmodal(){ //for button disabled and enable

if(newstockname() == true)
{
      document.getElementById("nstockbtn").disabled = false;
}
else
{
  document.getElementById("nstockbtn").disabled = true;
}
}   
// ============== END JS NEW STOCK MODAL ==================//
function Alert(){
    $('.close').click(function() {
     $('.alert').hide();
     })
     $('.alert').show()
     {
      $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
      $("#success-alert").slideUp(500);
        });
     }
  }


