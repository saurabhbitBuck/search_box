function Companyname(){
    
        var cname=document.getElementById("companyname").value;
        if(cname.length == 0)
            {
                document.getElementById("companyname").style=" border:1px solid red ";
                document.getElementById("cmperror").innerHTML="Supplier Name is required";
                document.getElementById("cmperror").style="visibility:visible;color:red";
                document.getElementById("companyname").value;
                return false;
            }
              document.getElementById("companyname").style=" ";
              document.getElementById("companyname").value;
              document.getElementById("cmperror").style="display:none";
              return true;
    }

    function Address(){
        
            var add = document.getElementById("add1").value;
            if(add.length == 0)
                {
                    document.getElementById("add1").style=" border:1px solid red ";
                    document.getElementById("adderror").innerHTML="Address is required";
                    document.getElementById("adderror").style="visibility:visible;color:red";
                    document.getElementById("add1").value;
                    return false;
                }
                  document.getElementById("add1").style=" ";
                  document.getElementById("add1").value;
                  document.getElementById("adderror").style="display:none";
                  return true;
        }
        function City(){
            
                var city = document.getElementById("city").value;
                if(city.length == 0)
                    {
                        document.getElementById("city").style=" border:1px solid red ";
                        document.getElementById("cityerror").innerHTML="City is required";
                        document.getElementById("cityerror").style="visibility:visible;color:red";
                        document.getElementById("city").value;
                        return false;
                    }
                      document.getElementById("city").style=" ";
                      document.getElementById("city").value;
                      document.getElementById("cityerror").style="display:none";
                      return true;
            }
        function Email() /* It validates Business Email field */
        {
          var email= document.getElementById("email").value;
          var atposition=email.indexOf("@");  
          var dotposition=email.lastIndexOf(".");  
            
          /*if(email.length == 0 ) /* Empty field */
         /* {
            document.getElementById("email").style=" border:1px solid red ";
            document.getElementById("emailerror").innerHTML="Business Email is required";
            document.getElementById("emailerror").style="visibility:visible;color:red";
            document.getElementById("email").value;
            return false;
          }*/
          if(atposition<1 || dotposition<atposition+2 || dotposition+2>=email.length)  /* Pattern of email */
          {
            document.getElementById("email").style=" border:1px solid red ";
            document.getElementById("emailerror").innerHTML="Enter valid email ";
            document.getElementById("emailerror").style="visibility:visible;color:red";
            document.getElementById("email").value;
            return false;
          }
          document.getElementById("email").style=" ";
          document.getElementById("email").value;
          document.getElementById("emailerror").style="display:none";
          return true;
        }
        
        function Fullname(){
            
                var name=document.getElementById("fullname").value;
                if(name.length == 0)
                    {
                        document.getElementById("fullname").style=" border:1px solid red ";
                        document.getElementById("nameerror").innerHTML="Full Name is required";
                        document.getElementById("nameerror").style="visibility:visible;color:red";
                        document.getElementById("fullname").value;
                        return false;
                    }
                      document.getElementById("fullname").style=" ";
                      document.getElementById("fullname").value;
                      document.getElementById("nameerror").style="display:none";
                      return true;
            }
        

        function checksupplier() /* It checks whether all required fields are filled on all Supplier page */
        {
            Address(); Companyname(); City();
          if(Companyname() == true && Address() == true && City()== true)
          {
              return true;
                 // window.location="suppliers.html";      
          }
          else if(Companyname()==false){ document.getElementById("companyname").focus();}
          else if(Address()==false){ document.getElementById("add1").focus();}
          else if(City()==false){ document.getElementById("city").focus();}
          else{return false;}
        }
        function checkcontact()
        {
            if(Fullname()==true)
            {
                return true;
                //window.location="supplier_detail.html";  
            }
            else if(Fullname()==false){ document.getElementById("fullname").focus();} 
            else{return false;}
        }



    function isNumberKey(evt) // for ristrict the user to enter only number in textbox in the form.
    {
      
       var charCode = (evt.which) ? evt.which : evt.keyCode;
       if (charCode != 46 && charCode > 31 
         && (charCode < 48 || charCode > 57))
          return false;
       return true;
    }
    function ValidateAlpha(evt) //for ristrict the user to enter only alphabets in textbox in the form.
    {
        var keyCode = (evt.which) ? evt.which : evt.keyCode
        if ((keyCode < 65 || keyCode > 90) && (keyCode < 97 || keyCode > 123) && keyCode != 32) 
        return false;
            return true;
    }
    function Alert(){
        $('.close').click(function() {
         $('.alert').hide();
         })
         $('.alert').show()
         {
          $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
          $("#success-alert").slideUp(500);
            });
         }
      }    
      