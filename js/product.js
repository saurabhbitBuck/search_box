
function Productname(){

    var productname=document.getElementById("productname").value;
    
    if(productname.length == 0)
        {
            document.getElementById("productname").style=" border:1px solid red ";
            document.getElementById("pnerror").innerHTML="Product Name is required";
            document.getElementById("pnerror").style="visibility:visible;color:red";
            document.getElementById("productname").value;
            return false;
        }
          document.getElementById("productname").style=" ";
          document.getElementById("productname").value;
          document.getElementById("pnerror").style="display:none";
         
          return true;
          
}

function Sku(){  
    var s=document.getElementById("sku").value;
  
    if(s.length == 0)
      {
        document.getElementById("sku").style=" border:1px solid red ";
        document.getElementById("skuerror").innerHTML="SKU is required";
        document.getElementById("skuerror").style="visibility:visible;color:red";
        document.getElementById("sku").value;
        return false;
      }
      document.getElementById("sku").style=" ";
      document.getElementById("sku").value;
      document.getElementById("skuerror").style="display:none";
     
      return true;
      
}

function Initialstock() {
 var is=document.getElementById("stock").value;
 
    if(is.length == 0)
      {
        document.getElementById("stock").style=" border:1px solid red ";
        document.getElementById("stockerror").innerHTML="Initial Stock is required";
        document.getElementById("stockerror").style="visibility:visible;color:red";
        document.getElementById("stock").value;
        return false;
      }
      document.getElementById("stock").style=" ";
      document.getElementById("stock").value;
      document.getElementById("stockerror").style="display:none";
     
      return true;
      
}   
    
function Initialcost() {

 var ic=document.getElementById("cost").value;
 
    if(ic.length == 0)
      {
        document.getElementById("cost").style=" border:1px solid red ";
        document.getElementById("costerror").innerHTML="Initial Cost is required";
        document.getElementById("costerror").style="visibility:visible;color:red";
        document.getElementById("cost").value;
        return false;
      }
      document.getElementById("cost").style=" ";
      document.getElementById("cost").value;
      document.getElementById("costerror").style="display:none";
     
      return true;
      
}
function isNumberKey(evt) // for ristrict the user to enter only number in textbox in the form.
{
  
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode != 46 && charCode > 31 
     && (charCode < 48 || charCode > 57))
      return false;
   return true;
}
function ValidateAlpha(evt) //for ristrict the user to enter only alphabets in textbox in the form.
{
    var keyCode = (evt.which) ? evt.which : evt.keyCode
    if ((keyCode < 65 || keyCode > 90) && (keyCode < 97 || keyCode > 123) && keyCode != 32)
     
    return false;
        return true;
}

function checkproductform() /* It checks whether all required fields are filled on all Product  page */
{
  Productname(); Sku(); Initialstock(); Initialcost();
  if(Productname()==true && Sku()== true && Initialstock()== true && Initialcost()== true)
  {
      return true;   
    //window.location="inventory.html";  
    //Alert();     
  }
  else if(Productname()==false){ document.getElementById("productname").focus();}
  else if(Sku()==false){ document.getElementById("sku").focus();}
  else if(Initialstock()==false){ document.getElementById("stock").focus();}
  else if(Initialcost()==false){ document.getElementById("cost").focus();}
  else{return false;}
}

function preview_image(event) /* Preview image on AddProductVariant page */
{
     var reader = new FileReader();
     reader.onload = function()
        {
          var output = document.getElementById('output_image');
          output.src = reader.result;
        }
          reader.readAsDataURL(event.target.files[0]);
}
// For Product Variant
function Variantname(){
  
      var vname=document.getElementById("variantname").value;
      
      if(vname.length == 0)
          {
              document.getElementById("variantname").style=" border:1px solid red ";
              document.getElementById("vnerror").innerHTML="Variant Name is required";
              document.getElementById("vnerror").style="visibility:visible;color:red";
              document.getElementById("variantname").value;
              return false;
          }
            document.getElementById("variantname").style=" ";
            document.getElementById("variantname").value;
            document.getElementById("vnerror").style="display:none";
           
            return true;
            
  }
  function checkproduct_variantform() /* It checks whether all required fields are filled on all Product Variant  page */
  {
    Variantname(); Sku(); Initialstock(); Initialcost();
    if(Variantname()==true && Sku()== true && Initialstock()== true && Initialcost()== true)
    {
            window.location="product_detail.php";      
    }
    else if(Variantname()==false){ document.getElementById("variantname").focus();}
    else if(Sku()==false){ document.getElementById("sku").focus();}
    else if(Initialstock()==false){ document.getElementById("stock").focus();}
    else if(Initialcost()==false){ document.getElementById("cost").focus();}
  }
  function Alert(){
    $('.close').click(function() {
     $('.alert').hide();
     })
     $('.alert').show()
     {
      $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
      $("#success-alert").slideUp(500);
        });
     }
  }


  
