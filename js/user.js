function Email() /* It validates Business Email field */
{
  var email= document.getElementById("email").value;
  var atposition=email.indexOf("@");  
  var dotposition=email.lastIndexOf(".");  
    
  if(email.length == 0 ) /* Empty field */
  {
    document.getElementById("email").style=" border:1px solid red ";
    document.getElementById("emailerror").innerHTML="Email is required";
    document.getElementById("emailerror").style="visibility:visible;color:red";
    document.getElementById("email").value;
    return false;
  }
  if(atposition<1 || dotposition<atposition+2 || dotposition+2>=email.length)  /* Pattern of email */
  {
    document.getElementById("email").style=" border:1px solid red ";
    document.getElementById("emailerror").innerHTML="Enter valid email ";
    document.getElementById("emailerror").style="visibility:visible;color:red";
    document.getElementById("email").value;
    return false;
  }
  document.getElementById("email").style=" ";
  document.getElementById("email").value;
  document.getElementById("emailerror").style="display:none";
  return true;
}

function Fullname(){
    
        var name=document.getElementById("fullname").value;
        if(name.length == 0)
            {
                document.getElementById("fullname").style=" border:1px solid red ";
                document.getElementById("nameerror").innerHTML="Full Name is required";
                document.getElementById("nameerror").style="visibility:visible;color:red";
                document.getElementById("fullname").value;
                return false;
            }
              document.getElementById("fullname").style=" ";
              document.getElementById("fullname").value;
              document.getElementById("nameerror").style="display:none";
              return true;
    }
    function checkuser()
    {
        Fullname(); Email();
        if(Fullname()==true && Email()==true)
        {
            return true;  
        }
        else if(Fullname()==false){ document.getElementById("fullname").focus();} 
        else if(Email()==false){ document.getElementById("email").focus();} 
        else return false;
    }
    function Alert(){
        $('.close').click(function() {
         $('.alert').hide();
         })
         $('.alert').show()
         {
          $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
          $("#success-alert").slideUp(500);
            });
         }
      }