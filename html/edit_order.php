<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- <meta http-equiv="ScreenOrientation" content="autoRotate:disabled">  -->
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Edit Orders</title>
  <!-- Bootstrap core CSS-->
  <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="../css/sb-admin.css" rel="stylesheet">
  <!-- Custom css-->
  <link href="../css/custom.css" rel="stylesheet">
  <script src="../vendor/jquery/jquery.min.js"></script>
  <script src="../js/sb-admin-datatables.min.js"></script>
  <!-- for calendar-->
  <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
  <style>
  
   abbr
{
  border-bottom: 1px dotted #666;
  cursor: help;
}
div.appended{
                font-size: 1vw;
            }
img.app-image {
  width: 100%; 
  
  height: 80%;
}

    </style>
 
</head>

<body  class="fixed-nav bg-dark" id="page-top" onload="myFunction()">
  
 
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav"><!-- Navigation-->
    <a class="navbar-brand" href="index.html">GENEX OMS</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link" href="dashboard.html">
            <i class="fa fa-fw fa-dashboard"></i>
            <span class="nav-link-text">Dashboard</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Inventory">
          <a class="nav-link" href="inventory.php">
            <i class="fa fa-cubes"></i>
            <span class="nav-link-text">Inventory</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Relationships">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents" data-parent="#exampleAccordion">
            <i class="fa fa-users" ></i>
            <span class="nav-link-text">Relationships</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseComponents">
            <li>
              <a href="customers.php">Customers</a>
            </li>
            <li>
              <a href="suppliers.php">Suppliers</a>
            </li>
          </ul>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Sales Orders">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseExamplePages" data-parent="#exampleAccordion">
            <i  class="fa fa-book"></i>
            <span class="nav-link-text">Sales Orders</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseExamplePages">
            <li>
              <a href="orders.html">Orders</a>
            </li>
            <li>
              <a href="invoices.html">Invoices</a>
            </li>
            <li>
              <a href="shipments.html">Shipments</a>
            </li>
            <li>
              <a href="Returns.html">Returns</a>
            </li>
          </ul>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Stock Keeping">
          <a class="nav-link" href="stock_keeping.html">
            <i class="fa fa-shopping-cart" ></i>
            <span class="nav-link-text">Stock Keeping</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Settings">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMulti" data-parent="#exampleAccordion">
            <i  class="fa fa-cog"></i>
            <span class="nav-link-text">Settings</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseMulti">
            <li>
              <a href="users.php">Users</a>
            </li>
            <li>
              <a href="configuration.php">Configuration</a>
            </li>
          </ul>
        </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Intelligence">
              <a class="nav-link" href="intelligence.html">
                <i class="fa fa-line-chart"></i>
                <span class="nav-link-text">Intelligence</span>
              </a>
            </li>
          </ul> <!--end of menu  -->
          <ul class="navbar-nav sidenav-toggler"> 
           <li class="nav-item">
             <a class="nav-link text-center" id="sidenavToggler">
               <i class="fa fa-fw fa-angle-left"></i>
             </a>
           </li>
         </ul><!-- end of down side arrow-->
         <ul class="navbar-nav ml-auto"><!--user profile-->
        <li class="nav-item dropdown">
             <a class="nav-link dropdown-toggle mr-lg-10 " id="messagesDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
              <img src="..\image\face.png" class="rounded-circle" width="30" height="30">
                <span >Harsha Shrikhande</span>
            </a>
            <div class="dropdown-menu" aria-labelledby="messagesDropdown">
              <a class="dropdown-item" href="user_profile.html">
                <div class="dropdown-message small">My Profile</div>
              </a> 
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="login.html">
                <div class="dropdown-message small">Logout</div>
              </a> 
          </li>
        </ul><!--end of user profile-->
       </div><!--end of top menu-->
  </nav>
  
  <div class="content-wrapper " >
    <div class="container-fluid">
      <div class="row" id="breadcrumb"><!--Breadcrumbs-->
        <div  class="col-sm-7 col-lg-7">
         <ol class="breadcrumb">
           <li class="breadcrumb-item active"><a href="orders.html">Orders</a></li>
           <li class="breadcrumb-item active"><a href="order_detail.html">#SO0001</a></li>
           <li class="breadcrumb-item active">Edit</li>
         </ol>
        </div> 
        <div class="col-sm-5 col-lg-5">
            <div class="d-flex flex-row-reverse">
                <button   type="button" class="my-2 mx-1 btn btn-primary">Save</button>
              
              <button   type="button" class="my-2 mx-1 btn btn-outline-secondary" onclick="window.location='order_detail.html';">Cancel</button>
              
            </div>
          </div>
     </div><!--end of Breadcrumbs-->
    
     <div class="body-font">
      <div class="row">
        <div class="col-sm-9"><!-- for left division -->
          <div class="card">
            <div class="card-header">
            <div class="row">
                <div class="col-sm-7">
                  <p class="order_subheading mt-1 ">Komal</p>
                  </div>
                <div class="col-sm-2 mt-2">
                    <select class="form-control form-control-sm" id="status">
                    <option value="#Status">Active</option>
                    <option value="#Status">Finalized</option>
                    </select>
                </div>
                 <div class="col-sm-3  mt-2">
                    <div class="input-group">
                        <input type="text" value="SO0001" class="form-control form-control-sm"/>
                        <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                    </div>
                 </div>
            </div><!-- End of card header -->
            </div> 
    <div class="row mt-2">
      <div class="col-sm-6">
          <table >
            <tr>
              <td class="order_label">Bill To:</td>
              <td >
                <input class="order_input" data-toggle="tooltip"  title="Abc House,Pune,Maharstra 411013" type="text" readonly="readonly" value="Pune Maharashtra"/>
                <!-- <textarea class="pd_input" type="text" readonly="readonly"  rows="3" cols="56">Telluric Technology Telluric Technology Telluric Technology Telluric Technology Telluric Technology Telluric Technology Telluric Technology Telluric Technology Telluric Technology Telluric Technology <abbr title="This text will show up in your tooltip!"></abbr></textarea> -->
              </td>
            </tr>
            <tr>
              <td class="order_label">Ship To:</td>
              <td>
                <!-- <textarea class="pd_input" type="text" readonly="readonly"maxlength="250" rows="5" cols="56"></textarea> -->
                <input class="order_input" data-toggle="tooltip"  title="Abc House,Pune,Maharstra 411013" type="text" readonly="readonly" value="Pune Maharashtra"/>
              </td>
            </tr>
            <tr>
              <td class="order_label">Ship From:</td>
              <td>
                <!-- <textarea class="pd_input" type="text" readonly="readonly" maxlength="250" rows="5" cols="56" /></textarea> -->
                <input class="order_input" data-toggle="tooltip"  title="Abc House,Pune,Maharstra 411013" type="text" readonly="readonly" value="Pune Maharashtra"/>
              </td>
            </tr>
            <tr>
              <td class="order_label">Assigned To:</td>
              <td>
                <!-- <textarea class="pd_input" type="text" readonly="readonly" maxlength="250" rows="5" cols="56" /></textarea> -->
                <!-- <input class="order_input" data-toggle="tooltip"  title="ABC XYZ" type="text" readonly="readonly" value="ABC XYZ"/> -->
                <select class="form-control form-control-sm">
                    <option value="abc">ABC</option>
                    <option value="xyz">XYZ</option>
                    <option value="pqr">PQR</option>
                    </select>
              </td>
            </tr>
            <tr>
              <td class="order_label">Price List:</td>
              <td>
                <!-- <textarea class="pd_input" type="text" readonly="readonly" maxlength="250" rows="5" cols="56" /></textarea> -->
                <select class="form-control form-control-sm">
                    <option value="abc">Wholesale</option>
                    <option value="xyz">Retail</option>
                    </select>
              </td>
            </tr>
          </table>
        </div>

        <div class="col-sm-6" >
          <table class="order">
            <tr>
              <td class="order_label">Issued Date:</td>
              <td>
              <div class='input-group date input-group-sm'>
                      <input id="datepicker" class="form-control form-control-sm" />
                    <span class="input-group-addon">
                        <span><i class="fa fa-calendar" aria-hidden="true" id="datepicker"></i></span>
                    </span>
				
              </div> 
              </td>
            </tr>
            <tr>
              <td class="order_label">Payment Date:</td>
              <td>
              <div class='input-group date input-group-sm'>
                      <input id="datepicker1" class="form-control form-control-sm"/>
                    <span class="input-group-addon">
                        <span><i class="fa fa-calendar" aria-hidden="true" id="datepicker1"></i></span>
                    </span>
                </div> 
              </td>
            </tr>
            <tr>
              <td class="order_label">Email:</td>
              <td>
                <input class="order_input" type="text" name="brand" value="Me@example.com" readonly="readonly" />
              </td>
            </tr>
            <tr>
              <td class="order_label">Phone:</td>
              <td>
                <input class="order_input" type="text" name="brand" value="0123456789" readonly="readonly" />
              </td>
            </tr>
            <tr>
              <td class="order_label">Total Are:</td>
              <td>
                <!-- <input class="order_input" type="text" name="brand" value="Tax Exclusive" readonly="readonly" /> -->
                <select class="form-control form-control-sm">
                    <option value="tax exclusive">Tax Exclusive</option>
                    <option value="tax inclusive ">Tax Inclusive</option>
                    </select>
              </td>
            </tr>
          </table>
        </div>
      </div>
        <br>
        <div class="table-responsive">
          <table class="table" id="edit_order" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th></th>
                <th>Product</th>
                <th>Quantity</th>
                <th>Available</th>
                <th>Price </th>
                <th>Discount</th>
                <th>Tax</th>
                <th>Total</th>
                <th></th>
              </tr>
            </thead>
            <tbody id="edit_order">
                
            <tr>
                <td><img src="../image/mobile.jpg" alt="img" height="40px" width="40px"></td>
                <td>Diamond Ring</td>
                <td><input class="form-control form-control-sm" type="number" min="1"/></td>
                <td>10</td>
                <td><input class="form-control form-control-sm" type="number" min="1"/> </td>
                <td><input class="form-control form-control-sm" type="number" min="1" placeholder="%"/></td>
                <td><input class="form-control form-control-sm" type="number" min="1" placeholder="%"/></td>
                <td>50000</td>
                <td><a class="nav-link" data-toggle="modal" data-target="#DeleteOrders"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
              </tr>
                
            </tbody>
      <!-- <tfoot>
            
          <tr>
          
            <td colspan="9">
            
              <span class="text-primary">
                +
              </span>
              <small>
                <a href="#" id="addInputField">
                  Add another item
                </a>
              </small>
            </td>
          </tr>
  </tfoot> -->
          </table>
          <hr>
          <div id="data" class="card h-25 w-75 d-none" style=" z-index:+1; position:absolute;">
              <div id="data1" class="h-100 w-100" style="overflow-y: auto;overflow-x:hidden">
              </div>
          </div>
          <span class="text-primary">
            +
          </span>
            <small>
              <a href="#" id="addInputField">
                  Add another item
              </a>
            </small>
        </div> <hr>

        <div class="form-group">
          <div class="row">
              <div class="col-sm-6">
                <table>
                  <tr><td id="msg"><label>Message to Customer</label></td></tr>
                  <tr><td id="msg"><textarea cols="40" rows="6" id="txt" style="resize: none;" >Lorem ipsum dolor sit amet, consectetur adipisicing elit,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</textarea></td></tr>
                </table>  
              </div>
          <div class="col-sm-6" >
            <!-- <label class="order">Total Units :</label><span>12</span><br>
            <label class="order">Subtotal :</label><span>12</span><br>
            <label class="order">Plus VAT(12.5%):</label><span>12</span><br>
            <label class="order">Total:</label><span>12</span> -->
            
              <table class="order" >
                  <tr>
                      <td class="order_label">Total Units:</td>
                      <td><input class="order_input" size= type="text" name="username" value="3" readonly="readonly" /></td>
                      
                  </tr>
                  
                  <tr>
                      <td class="order_label">Subtotal:</td>
                      <td ><input class="order_input" type="text" name="password" value="225.00" readonly="readonly" /></td>
                  </tr>
                  <tr>
                      <td class="order_label">Plus VAT(12.5%):</td>
                      <td><input class="order_input"  type="text" name="password" value="3.13" readonly="readonly" /></td>
                  </tr>
                  <tr>
                      <td class="order_label"><strong>Total:</strong></td>
                      <!-- <td><span><b>240.23</b></span></td> -->
                       <td><input class="order_input" type="text" name="password" value="240.13" readonly="readonly" /></td> 
                  </tr>
              </table>
          </div>
          </div>
      </div>
    </div> <!-- end of card -->
    </div> <!-- end of division left -->
  <!-- </div> End of row col sm 9 -->

  <div class="col-sm-3" id="leftspace">
  <div class="card right1space">
    <div class="card-header">
        <div class="row">
            <div class="col-sm-7 rightspace"><a href="invoices.html"><p id="Pd_subheading">Invoices</p></a></div>
            <div class="col-sm-5 rightspace rightdata"><p>Invoiced</p></div>
          </div>
    </div>
    <div class="card-body">
      <div class="row">
          <div class="col-sm-7 rightspace rightdata1"><p><a href="invoices.html">SO0001</a></p></div>

          <div class="col-sm-5 rightspace rightdata1"><p>900.00</p></div>
        </div>
        <button onClick="window.location='new_invoice.html';" type="button" class="my-2 mx-1 btn btn-primary btn-block">New Invoice</button>
        <!-- <a href="#" class="btn btn-primary btn-block">New Invoice</a> -->
    </div>
  </div>
   
  
  <div class="card right1space">
      <div class="card-header">
          <div class="row">
              <div class="col-sm-7 rightspace"><a href=""><p id="Pd_subheading">Payments</p></a></div>
              <div class="col-sm-5 rightspace rightdata"><p>900.00 to pay</p></div>
          </div>
      </div>
      <div class="card-body">
          <div class="row">
              <div class="col-sm-7 rightspace rightdata1"><p>Payment for SO0001</p></div>
              <div class="col-sm-5 rightspace rightdata1"><p>500.00</p></div>
              <div class="col-sm-7 rightspace rightdata1"><p>Payment for SO0002</p></div>
              <div class="col-sm-5 rightspace rightdata1"><p>400.00</p></div>
          </div>
          <button  type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#PaymentModal">Payment</button>
          <!-- <a href="#" class="btn btn-primary btn-block">Payment</a> -->
      </div>
  </div>

  
  <div class="card right1space">
        <div class="card-header">
            <div class="row">
                <div class="col-sm-7 rightspace"><a href="shipments.html"><p id="Pd_subheading">Shipments</p></a></div>
                <div class="col-sm-5 rightspace rightdata"><p>2 to fulfill</p></div>
            </div>
          </div>
              <!-- <button ><a href="invoices.html">Invoices</a></button>  -->
              <div class="card-body">
                  <div class="row">
                      <div class="col-sm-7 rightspace rightdata1"><p><a href="shipments.html">Shipments#1</a></p></div>
                      <div class="col-sm-5 rightspace rightdata1"><p>900.00</p></div>
                  </div>
                  <button onClick="window.location='new_shipment.html';" type="button" class="my-2 mx-1 btn btn-primary btn-block">Pack</button>
              </div>
       
  </div>

 <div class="card right1space">
      <div class="card-header rightspace"><p id="Pd_subheading"><a href="returns.html">Returns</a></p></div>
          <div class="card-body">
                    <div class="row">
                        <div class="col-sm-2 rightspace"><i class="fa fa-plus" aria-hidden="true"></i></div>
                        <div class="col-sm-10 rightspace"><p>Create a partial return or return all of the products in this order.</p></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2 rightspace"> <i class="fa fa-check" aria-hidden="true"></i></div> 
                        <div class="col-sm-10 rightspace"><p>Once returned, set the date received to increase stock levels.</p></div>
                     </div>
                    <div class="row">
                      <div class="col-sm-5 rightspace">
                     <button onClick="window.location='';" type="button" class="my-2 mx-1 btn btn-secondary btn-block">Manual</button>
                     </div>
                     <div class="col-sm-7 rightspace">
                     <button onClick="window.location='';" type="button" class="my-2 mx-1 btn btn-primary btn-block">Return All</button>
                     </div>
                    </div>
          </div>
      </div>
  </div>  <!--end of card -->
  </div><!-- End of row  -->
</div> <!-- End of body-font  -->
     
        <!-- Delete Modal-->
  <div class="modal fade" id="DeleteOrders" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Delete Order?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Are you sure you want to delete this order?</div>
        <div class="modal-footer">
          <button   class="btn btn-outline-danger btn-sm" type="button" data-dismiss="modal">Delete</button>
          <button type="button" class="btn btn-outline-secondary btn-sm" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>
   
  <!-- status Modal-->
  <div class="modal fade" id="Status" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Change Status?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Are you sure you want to change the status?</div>
          <div class="modal-footer">
            <button   class="btn btn-outline-danger btn-sm" type="button" data-dismiss="modal">Change</button>
            <button type="button" class="btn btn-outline-secondary btn-sm" data-dismiss="modal">Cancel</button>
          </div>
        </div>
      </div>
    </div>
          <!-- ========================== edit PRICE MODAL =================================-->
          <!-- <div class="modal fade" id="PaymentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
       <div class="modal-dialog" role="document">
         <div class="modal-content">
           <div class="modal-header">
             <h5 class="modal-title" id="exampleModalLabel">Create Payment</h5>
             <button class="close" type="button" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">×</span>
             </button>
 
           </div>
           <form id="price_list_form" action="edit_price_modal.php" method="POST">
           <div class="modal-body">
               <div class="form-group">
                   <label class="control-label" for="name">Payment Type</label>
                   <input type="text" maxlength="30" class="form-control form-control-sm" name="type" id="pricename"  
                   onblur="price_name();" onkeyup="checkpricemodal();"/> 
                   <span id="priceerror"></span>
                 </div>
                 <div class="form-group">
                     <label class="control-label" for="code">INR</label>
                     <input type="text" maxlength="30" class="form-control form-control-sm" name="inr" id="Code"/>
                     <span id="codeerror"></span> 
                 </div>
                 <div class="form-group">
                     <label class="control-label" for="priceid">Paid On</label>
                     <input type="text" maxlength="30" class="form-control form-control-sm" name="paid" id="priceid" />
                     <span id="codeerror"></span> 
                 </div>
                 <div class="form-group">
                     <label class="control-label" for="priceid">Reference</label>
                     <input type="text" maxlength="30" class="form-control form-control-sm" name="reference" id="priceid" />
                     <span id="codeerror"></span> 
                 </div>
           </div>
           <div class="modal-footer">
               <button class="btn btn-outline-secondary btn-sm" type="button" data-dismiss="modal">Cancel</button>
               <button type="button" class="btn btn-outline-danger btn-sm" id="delete_price_btn"  data-dismiss="modal" >Create Payment</button>
             <!-- <button class="btn btn-primary btn-sm" id="pricebtn" disabled>Save</button> -->
             <!-- </div>
             </form>
           </div>
         </div>
       </div>  -->
       <!-- =====================================END OF edit PRICE MODAL =================================-->
     
      </div><!-- /.container-fluid-->
    </div><!-- /.content-wrapper-->
   
    
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Bootstrap core JavaScript-->
    <script src="../vendor/jquery/jquery.min.js"></script>
    <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="../vendor/datatables/jquery.dataTables.js"></script>
    <script src="../vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="../js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="../js/sb-admin-datatables.min.js"></script>
    	<!--for calendar-->
<script src="../js/jquery-ui.min.js"></script> 
<script src="../js/calender.js"></script> 

<!-- For tooltip -->

  <script>
    $(document).ready(function() {	

/**
* store the value of and then remove the title attributes from the
* abbreviations (thus removing the default tooltip functionality of
     * the abbreviations)
*/
$('abbr').each(function(){		

$(this).data('title',$(this).attr('title'));
$(this).removeAttr('title');

});

    /**
* when abbreviations are mouseover-ed show a tooltip with the data from the title attribute
*/	
$('abbr').mouseover(function() {		

// first remove all existing abbreviation tooltips
$('abbr').next('.tooltip').remove();

// create the tooltip
$(this).after('<span class="tooltip">' + $(this).data('title') + '</span>');

// position the tooltip 4 pixels above and 4 pixels to the left of the abbreviation
var left = $(this).position().left + $(this).width() + 4;
var top = $(this).position().top - 4;
$(this).next().css('left',left);
$(this).next().css('top',top);				

});

/**
* when abbreviations are clicked trigger their mouseover event then fade the tooltip
* (this is friendly to touch interfaces)
*/
$('abbr').click(function(){

$(this).mouseover();

// after a slight 2 second fade, fade out the tooltip for 1 second
$(this).next().animate({opacity: 0.9},{duration: 2000, complete: function(){
  $(this).fadeOut(1000);
}});

});

/**
* Remove the tooltip on abbreviation mouseout
*/
$('abbr').mouseout(function(){
  
$(this).next('.tooltip').remove();				

});	

});

$("#status").change(function () {
    if ($(this).val() == "#Status") {
        $('#Status').modal('show');
      }
    if ($(this).val() == "#DeleteShipments") {
        $('#DeleteShipments').modal('show');
      }
    if ($(this).val() == "#consumergoods3") {
        $('#consumergoods3').modal('show');
      }
    if ($(this).val() == "#consumergoods4") {
        $('#consumergoods4').modal('show');
      }
 });


    </script>
</body>
</html>
